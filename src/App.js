import React from 'react';
import logo from './logo.svg';
import './css/App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Liebe ist für alle da</h1>
      </header>
    </div>
  );
}

export default App;
